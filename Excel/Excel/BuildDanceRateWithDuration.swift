//
//  BuildDanceRateWithDuration.swift
//  Excel
//
//  Created by Aviel Gross on 25/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

class BuildDanceRateWithDuration {
    
    class func run(dancersSheet dancers: BRAWorksheet) {
        
        
        // for each return#(W) at each trialID(I):
        // collect all o and s couples
        // sum all durations from s to o for each couple
        // sum # of circuits from all o rows
        // divide duration sum by circuit sum
        // export the last O with: sum duration | sum circuits | rate
        
        let rows = Array(dancers.rows) as! [BRARow]
        
        
        var exportRows = [Row]()
        
        let titleRow: Row = Utils.titleRow(from: dancers, prefixCellTitles: ["Sum Dance Duration", "Sum Circuits", "Dance Rate Per Bout"])
        exportRows.append(titleRow)
        
        
        var trialID: Int = -1
        var returnNum: Int = -1
        var sRows: [BRARow] = []
        var oRows: [BRARow] = []

        
        for (rowIndex, row) in rows.dropFirst().enumerated() where row.cells.count > 3 {
//            if row.rowIndex == 69 || row.rowIndex == 70 {
//                print("wait")
//            }
            let eventCell = row.cells[4] as! BRACell
            let event = eventCell.stringValue()!.lowercased()
            guard event == "s" || event == "o" else { continue }
            
            let rowTrialID = dancers.cell(forCellReference: "K\(row.rowIndex)").integerValue()
            let rowReturnNum = dancers.cell(forCellReference: "Y\(row.rowIndex)").integerValue()
            if trialID == -1 {
                trialID = rowTrialID
                returnNum = rowReturnNum
            }
            
            if rowTrialID != trialID || rowReturnNum != returnNum {
                
                // update for new day/return#
                trialID = rowTrialID
                returnNum = rowReturnNum
                
                guard oRows.count > 0 else {
                    print("\(rowIndex): none")
                    continue
                }
                print("\(rowIndex): [SOME]")
                // new day/return# --> calc for current o-s couples
                assert(oRows.count == sRows.count)
                
                var sumTime = 0.0
                var sumCircuits = 0
                
                for i in [Int](0 ..< sRows.count) {
                    let oRow = oRows[i]
                    let sRow = sRows[i]
                    
                    // time diff from o to s
                    let oRowRealTimeFloat = (oRow.cells[0] as! BRACell).floatValue()
                    let totalORowTime = Utils.getTotalSeconds(from: oRowRealTimeFloat)
                    
                    let sRowRealTimeFloat = (sRow.cells[0] as! BRACell).floatValue()
                    let totalSRowTime = Utils.getTotalSeconds(from: sRowRealTimeFloat)
                    
                    sumTime += totalORowTime - totalSRowTime
                    
                    // circuits in o
                    guard let circuits = dancers.cell(forCellReference: "G\(oRow.rowIndex)")?.integerValue()
                        else { assertionFailure(); continue }
                    sumCircuits += circuits
                }
                
                let rate = Double(sumCircuits) / sumTime
                
                let oCellsStr: [String] = oRows.last!.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
                exportRows.append( ["\(sumTime)", "\(sumCircuits)", "\(rate)"] + oCellsStr )
                
                sRows = []
                oRows = []
            }

            
            if event == "s" {
                assert(oRows.count == sRows.count)
                sRows.append(row)
            } else if event == "o" {
                assert(oRows.count + 1 == sRows.count)
                oRows.append(row)
            }

            
        }
        

        export(rows: exportRows)
        
    }
    
    class func export(rows: [Row]) {
        let excelRows: [ExcelExport.ExcelRow] = rows.enumerated().map{ rowIndex, row -> ExcelExport.ExcelRow in
            return row.enumerated().map{ index, value in
                var atts: [ExcelExport.TextAttribute] = rowIndex == 0 ? [.height(60), .bold] : []
                
                switch index {
                case 3: fallthrough
                case 8: fallthrough
                case 11:
                    atts.append(.format("[h]:mm:ss.000"))
                    
                case 12: fallthrough
                case 16:
                    atts.append(.format("dd-mmm-yy"))
                    
                default:
                    break
                }
                
                return ExcelExport.ExcelCell(value, atts)
            }
        }
        
        ExcelExport.export(rows: excelRows, fileName: "dance rate and duration per entrance") { _ in
            print("DONE! 🍻")
        }
        
    }

    
}
