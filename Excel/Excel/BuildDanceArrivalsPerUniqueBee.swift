//
//  BuildDanceArrivalsPerUniqueBee.swift
//  Excel
//
//  Created by Aviel Gross on 26/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

class BuildDanceArrivalsPerUniqueBee {
    
    class func run(dancers: BRAWorksheet, feeders: BRAWorksheet, onlyAfter: Bool) {
        
        // for each "unique trial id" with each stage(M):
        
        // in dancers: find event code "f"
        // take the first row with "f" event code
        // take the unique trial ID & realtime
        
        // search that id in "feeders" only if it's after the realtime we took
        // if we have a match: add "arrived" to row begining
        // if doesn't add "didn't arrive"
        
        let dancersRows = Array(dancers.rows) as! [BRARow]
        
        //================= MATCH IN FEEDER =================
        func matchRowsStages(id: String, stage: String, after: Float) -> Bool {
            let feedersRows = Array(feeders.rows) as! [BRARow]
            
            print("matching \(id)[\(stage)] after \(after)")
            let match = feedersRows.lazy.filter { row in
                    (!onlyAfter || (row.cells[0] as! BRACell).floatValue() > after)
                    && (row.cells[4] as! BRACell).stringValue() == stage
                    && (row.cells[8] as! BRACell).stringValue() == id
            }.first
            print("match: \(match != nil ? "YES" : "no")")
            
            return match != nil
        }
        //===================================================
        
        var exportRows: [Row] = []
        
        // title row
        let titleRow: Row = Utils.titleRow(from: dancers, prefixCellTitles: ["Arrival"])
        exportRows.append(titleRow)
        
        
        var currentBeeTrialID: String = ""
        var currentStage: String = ""
        
        
        for row in dancersRows {

            guard row.cells.count > 7 else {
                print("short row: \(Utils.rowDescription(row))")
                continue
            }
            
            // is F row?
            guard (row.cells[4] as! BRACell).stringValue().uppercased() == "F"
                else { continue }
            
            let uniqueId = (row.cells[2] as! BRACell).stringValue()!
            let stage = dancers.cell(forCellReference: "M\(row.rowIndex)").stringValue()!
            
            // is it a different id or stage?
            guard uniqueId != currentBeeTrialID || stage != currentStage
                else { continue }
            
            currentBeeTrialID = uniqueId
            currentStage = stage
            
            print("\(row.rowIndex)")
            
            // get info from row
            let realTime = (row.cells[0] as! BRACell).floatValue()
            
            
            // check if matches in "feeders" and make match & stage value
            let match = matchRowsStages(id: uniqueId, stage: stage, after: realTime)
            let matchValue = match ? "Arrived" : "Didn't Arrive"
            
            // make row from match value and other cells and append for export
            let cellsStr: [String] = row.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
            let exportRow: Row = [matchValue] + cellsStr // arrived  | other cells
            exportRows.append(exportRow)
        }
        
        export(rows: exportRows, onlyAfter)
    }
    
    class func export(rows: [Row], _ onlyAfter: Bool) {
        let excelRows: [ExcelExport.ExcelRow] = rows.enumerated().map{ rowIndex, row -> ExcelExport.ExcelRow in
            return row.enumerated().map{ index, value in
                var atts: [ExcelExport.TextAttribute] = rowIndex == 0 ? [.height(60), .bold] : []
                
                switch index {
                case 1: fallthrough
                case 6: fallthrough
                case 9:
                    atts.append(.format("[h]:mm:ss.000"))
                    
                case 10: fallthrough
                case 14:
                    atts.append(.format("dd-mmm-yy"))
                    
                default:
                    break
                }
                
                return ExcelExport.ExcelCell(value, atts)
            }
        }
        
        ExcelExport.export(rows: excelRows, fileName: "dance arrivals per unique bee\(onlyAfter ? " - only after" : "")") { _ in
            print("DONE! 🍻")
        }
        
    }

}
