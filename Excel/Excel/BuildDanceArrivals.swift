//
//  BuildDanceArrivals.swift
//  Excel
//
//  Created by Aviel Gross on 21/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

class BuildDanceArrivals {
    
    class func run(dancers: BRAWorksheet, feeders: BRAWorksheet, onlyAfter: Bool) {
        
        // for each trial date F:
        
        // in dancers: find event code "f"
        // for each row with "f", we take the ID & realtime
        
        // search that id in "feeders" only if it's after the realtime we took
        // if we have a match: add "arrived" to row begining
        // if doesn't add "didn't arrive"
        
        let dancersRows = Array(dancers.rows) as! [BRARow]
        
        func matchRowsStages(id: String, after: Float, inDay: Float) -> String? {
            let feedersRows = Array(feeders.rows) as! [BRARow]

            print("matching \(id) in day \(inDay) after \(after)")
            let matches = feedersRows.filter { row in
                row.cells.count > 6
                && (!onlyAfter || (row.cells[0] as! BRACell).floatValue() > after)
                && (row.cells[6] as! BRACell).floatValue() == inDay
                && (row.cells[2] as! BRACell).stringValue() == id
            }
            
            print("\(matches.count) matches")
            
            let stageCells = matches.map { match -> BRACell in
                Array(match.cells).at(4) as! BRACell
            }
            let stageValues = stageCells.map(Utils.cellValue).filter{ $0.isEmpty == false }
            
            if stageValues.contains("Learning") && stageValues.contains("Test") {
                return "Both"
            }
            return stageValues.first
        }
        
        var exportRows: [Row] = []
        
        // title row
        let titleRow: Row = Utils.titleRow(from: dancers, prefixCellTitles: ["Arrival", "Arrival Stage"])
        exportRows.append(titleRow)

        
        
        
        for row in dancersRows {
            guard row.cells.count > 7 else {
                print("short row: \(Utils.rowDescription(row))")
                continue
            }
            
            guard (row.cells[2] as! BRACell).stringValue().uppercased() == "F"
                else { continue }
            
            print("\(row.rowIndex)")
            
            // get info from row
            let id = (row.cells[1] as! BRACell).stringValue()!
            let day = dancers.cell(forCellReference: "I\(row.rowIndex)").integerValue()
            let realTime = (row.cells[0] as! BRACell).floatValue()
            
            
            // check if matches in "feeders" and make match & stage value
            let match = matchRowsStages(id: id, after: realTime, inDay: Float(day))
            let matchValue = match != nil ? "Arrived" : "Didn't Arrive"
            let stageValue = match ?? ""
            
            // make row from match value and other cells and append for export
            let cellsStr: [String] = row.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
            let exportRow: Row = [matchValue, stageValue] + cellsStr // arrived | stage | other cells
            exportRows.append(exportRow)
        }
        
        export(rows: exportRows, onlyAfter)
    }
    
    class func export(rows: [Row], _ onlyAfter: Bool) {
        let excelRows: [ExcelExport.ExcelRow] = rows.enumerated().map{ rowIndex, row -> ExcelExport.ExcelRow in
            return row.enumerated().map{ index, value in
                var atts: [ExcelExport.TextAttribute] = rowIndex == 0 ? [.height(60), .bold] : []
                
                switch index {
                case 2: fallthrough
                case 5: fallthrough
                case 8:
                    atts.append(.format("[h]:mm:ss.000"))
                    
                case 9: fallthrough
                case 12:
                    atts.append(.format("dd-mmm-yy"))
                    
                default:
                    break
                }
                
                return ExcelExport.ExcelCell(value, atts)
            }
        }
        
        ExcelExport.export(rows: excelRows, fileName: "dance arrivals\(onlyAfter ? " - only after" : "")") { _ in
            print("DONE! 🍻")
        }
        
    }

    
}
