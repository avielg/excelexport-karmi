//
//  ExcelExport.swift
//  Hours
//
//  Created by Aviel Gross on 18/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

private extension NSColor {
    
    /// Hex string of a UIColor instance.
    ///
    /// - Parameter includeAlpha: Whether the alpha should be included.
    /// - Returns: HEX, including the '#'
    func hexString(_ includeAlpha: Bool = false) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        if (includeAlpha) {
            return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }
    
}

class ExcelExport {
    
    enum TextAttribute {
        case backgroundColor(NSColor)
        case foregroundColor(NSColor)
        case bold
        case format(String)
        case height(Int)
        
        var parsed: String {
            switch self {
            case .backgroundColor(let c): return "background-color: \(c.hexString())"
            case .foregroundColor(let c): return "color: \(c.hexString())"
            case .bold: return "font-weight: bold"
            case .format(let s): return "mso-number-format: '\(s)'"
            case .height(let h): return "height: \(h)px"
            }
        }
    }

    struct ExcelCell {
        let value: String
        let attributes: [TextAttribute]
        let colspan: Int?
        
        init(_ value: String) {
            self.value = value
            attributes = []
            colspan = nil
        }
        
        init(_ value: String, _ attributes: [TextAttribute], colspan: Int? = nil) {
            self.value = value
            self.attributes = attributes
            self.colspan = colspan
        }
    }
    
    typealias ExcelRow = [ExcelCell]
    
    
    private class func styleValue(for textAttributes: [TextAttribute]) -> String {
        guard textAttributes.count > 0 else { return "" }
        
        let parsedAttributes = textAttributes.map{ $0.parsed }
        let style = parsedAttributes.joined(separator: "; ")
        return "style=\"\(style)\""
    }
    
    class func export(rows: [ExcelRow], fileName: String, done: @escaping (URL?)->Void) {
        let file = fileUrl(name: fileName)
        
        
        // build rows
        let rowsValue = rows.map{ row -> String in
            //let style =
            return row.map{ cell -> String in
                let cols = cell.colspan.map{ " colspan=\"\($0)\"" } ?? ""
                let style = styleValue(for: cell.attributes)
                return "<td\(cols) \(style)>\(cell.value)</td>"
                }.reduce("<tr>", { "\($0)\($1)" })
                .appending("</tr>")
            }.reduce("", { r, e in "\(r)\(e)"})
        
        
        // build file content
        let worksheetName = fileName
        let templatePre = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><head><meta http-equiv=Content-Type content='text/html; charset=UTF-8'><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>\(worksheetName)</x:Name><x:WorksheetOptions><x:Print><x:GridLines/></x:Print></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>"
        let templatePost = "</table border=\"1\"></body></html>"
        
        let content = "\(templatePre)\(rowsValue)\(templatePost)"
        
        // write content to file
        do {
            try content.write(to: file, atomically: true, encoding: .utf8)
            print("\(rows.count) Lines written to file")
            done(file) // call done
        } catch {
            print("Can't write \(rows.count) to file! [\(error)]")
            done(nil) // call done
        }
    }
    
    class func fileUrl(name: String) -> URL {
        let path = "/Users/aviel/Developer/Private-xcode/excelexport-karmi/Docs-Exported"
        let urlDir = URL(fileURLWithPath: path, isDirectory: true)
        return urlDir.appendingPathComponent("\(name).xls")
    }
    
}

