//
//  Utils.swift
//  Excel
//
//  Created by Aviel Gross on 21/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

extension Collection {
    /// Returns the element at the specified index
    ///     iff it is within bounds, otherwise nil.
    public func at(_ index: Index) -> Iterator.Element? {
        return index >= startIndex && index < endIndex
            ? self[index]
            : nil
    }
}

class Utils {
    
    class func titleRow(from sheet: BRAWorksheet, prefixCellTitles: [String]) -> Row {
        let rows = Array(sheet.rows) as! [BRARow]
        let titlesRow = rows[0].cells.map{ $0 as! BRACell }.map{ $0.stringValue()! }
        return prefixCellTitles + titlesRow
    }
    
    class func rowDescription(_ row: BRARow) -> String {
        let sCellsStr: [String] = row.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
        return sCellsStr.joined(separator: " | ")
    }
    
    class func cellValue(_ cell: BRACell) -> String {
        return cell.value ?? cell.stringValue() ?? ""
    }
    
    class func getTotalSeconds(from: Float) -> Double {
        return Double(from * 24 * 60 * 60 * 1000) / 1000
    }

    class func getTotalSeconds(from: String) -> Double? {
        let pointComps = from.components(separatedBy: ".")
        guard pointComps.count > 1 else { return nil }
        
        let floatingPoint = pointComps[1]
        let timeComps = pointComps[0].components(separatedBy: ":")
        
        let hour = Int(timeComps[0])!
        let minute = Int(timeComps[1])!
        let seconds = Int(timeComps[2])!
        
        let totalSeconds = seconds + (minute * 60) + (hour * 60 * 60)
        
        return Double(totalSeconds) + Double(floatingPoint)! / 1000
    }

}
