//
//  BuildDanceRate.swift
//  Excel
//
//  Created by Aviel Gross on 21/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation


class BuildDanceRate {
    
    
    class func run(dancersSheet: BRAWorksheet) {
        
        let rows = Array(dancersSheet.rows) as! [BRARow]
        
        var sRow: BRARow?
        var oRow: BRARow?
        
        
        var exportRows = [Row]()
        
        let titleRow: Row = Utils.titleRow(from: dancersSheet, prefixCellTitles: ["Dance Rate Per Bout"])
        exportRows.append(titleRow)
        

        for (_, row) in rows.dropFirst().enumerated() where row.cells.count > 3 {
            let eventCell = row.cells[2] as! BRACell
            let event = eventCell.stringValue()!

            if sRow == nil && oRow == nil && event.lowercased() == "s" {
                sRow = row
            } else if sRow != nil && event.lowercased() == "o" {
                oRow = row
                
                
                let oRowRealTimeFloat = (oRow!.cells[0] as! BRACell).floatValue()
                let totalORowTime = Utils.getTotalSeconds(from: oRowRealTimeFloat)
                
                
                let sRowRealTimeFloat = (sRow!.cells[0] as! BRACell).floatValue()
                let totalSRowTime = Utils.getTotalSeconds(from: sRowRealTimeFloat)
                
                
                
                let time = totalORowTime - totalSRowTime
                
                guard let circuits = dancers.cell(forCellReference: "E\(oRow!.rowIndex)")?.integerValue() else { continue }
                
                let rate = Double(circuits) / time
                
                
                let sCellsStr: [String] = sRow!.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
                //print(Utils.rowDescription(sRow!))
                
                let oCellsStr: [String] = oRow!.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
                //print(Utils.rowDescription(oRow!))
                
                //print("\(rate)\n\n")
                
                exportRows.append( [" "] + sCellsStr[0 ... 3] + [" "] + sCellsStr[4 ..< sCellsStr.count])
                exportRows.append( ["\(rate)"] + oCellsStr )
                
                oRow = nil
                sRow = nil
            }
        }
        
        export(rows: exportRows)
    }
    
    class func export(rows: [Row]) {
        let excelRows: [ExcelExport.ExcelRow] = rows.enumerated().map{ rowIndex, row -> ExcelExport.ExcelRow in
            return row.enumerated().map{ index, value in
                var atts: [ExcelExport.TextAttribute] = rowIndex == 0 ? [.height(60), .bold] : []

                switch index {
                case 1: fallthrough
                case 4: fallthrough
                case 7:
                    atts.append(.format("[h]:mm:ss.000"))
                    
                case 8: fallthrough
                case 11:
                    atts.append(.format("dd-mmm-yy"))
                    
                default:
                    break
                }
                
                return ExcelExport.ExcelCell(value, atts)
            }
        }
        
        ExcelExport.export(rows: excelRows, fileName: "dance rate per bout") { _ in
            print("DONE! 🍻")
        }
        
    }


}
