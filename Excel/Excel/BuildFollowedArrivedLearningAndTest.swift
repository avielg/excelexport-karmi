//
//  BuildFollowedArrivedLearningAndTest.swift
//  Excel
//
//  Created by Aviel Gross on 04/12/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

class BuildFollowedArrivedLearningAndTest {
    
    
    class func run(dancers: BRAWorksheet, feeders: BRAWorksheet, onlyAfter: Bool) {
        
        // for each "unique trial id" with each stage(M):
        
        // in dancers: find event code "f"
        // take the first row with "f" event code
        // take the unique trial ID & realtime
        
        // search that id in "feeders" only if it's after the realtime we took
        // if we have a match: add "arrived" to row begining
        // if doesn't add "didn't arrive"
        
        let dancersRows = Array(dancers.rows) as! [BRARow]
        
        //================= MATCH IN FEEDER =================
        func matchRowsStages(id: String, stage: String, after: Float) -> Bool {
            let feedersRows = Array(feeders.rows) as! [BRARow]
            
            //print("matching \(id)[\(stage)] after \(after)")
            let match = feedersRows.lazy.filter { row in
                (!onlyAfter || (row.cells[0] as! BRACell).floatValue() > after)
                    && (row.cells[4] as! BRACell).stringValue() == stage
                    && (row.cells[8] as! BRACell).stringValue() == id
                }.first
            //print("match: \(match != nil ? "YES" : "no")")
            
            return match != nil
        }
        //===================================================
        
        var exportRows: [Row] = []
        
        // title row
        let matchedForTestStageTitle = "Follow Arrived Learning & Test"
        let weirdTextTitle = "Followed and Arrived Learning and Follow Again Test"
        let numOfFsTitle = "Number of Times Followed Test"
        let initialColumnTitle = "Arrival After Following LEARNING Stage one line per stage"
        let prefixTitles = [matchedForTestStageTitle, weirdTextTitle, numOfFsTitle, initialColumnTitle]
        let titleRow: Row = Utils.titleRow(from: dancers, prefixCellTitles: prefixTitles)
        exportRows.append(titleRow)
        
        
        var currentBeeTrialID: String = ""
        var currentBeeTrialIdFRows: [BRARow] = []
        
        // Handle all F rows for certain unique trial ID bee
        func handleBeeInfo(fRows: [BRARow]) {
            
            guard fRows.count > 0 else { return }
            print("\(currentBeeTrialID) total: \(fRows.count)")
            
            // first row with Learning
            let firstF = fRows.first { row in
                let stage = (row.cells[12] as! BRACell).stringValue()!
                return stage == "Learning"
            }
            
            // skip bees with only Test F rows
            guard firstF != nil else { return }
            
            // get info from row
            let realTime = (firstF!.cells[0] as! BRACell).floatValue()
            let uniqueId = (firstF!.cells[2] as! BRACell).stringValue()!
            
            // check if matches in "feeders" and make match & stage value
            let match = matchRowsStages(id: uniqueId, stage: "Learning", after: realTime)
            
            
            // count F's in Test for this match
            let numOfTestFs = fRows.filter {
                let stage = ($0.cells[12] as! BRACell).stringValue()!
                return stage == "Test"
            }.count
            let weirdText = numOfTestFs > 0 ? "Followed Again Test" : "Didn't Follow Again Test"

            // check if matches in "feeders" for "Test" stage with first F row of 'Test'
            let testMatchValue: String
            if numOfTestFs > 0 {
                let firstTestF = fRows.first { row in
                    let stage = (row.cells[12] as! BRACell).stringValue()!
                    return stage == "Test"
                }
                let firstTestFRealTime = (firstTestF!.cells[0] as! BRACell).floatValue()
                let testMatch = matchRowsStages(id: uniqueId, stage: "Test", after: firstTestFRealTime)
                testMatchValue = testMatch ? "Arrived" : "Didn't Arrive"
            } else {
                testMatchValue = " " // no F rows for 'Test' stage
            }
            
            print("Test Fs: \(numOfTestFs) (total: \(fRows.count))")
            
            // match value for export
            let matchValue = match ? "Arrived" : "Didn't Arrive"
            
            // make row from match value and other cells and append for export
            let cellsStr: [String] = firstF!.cells.map{ $0 as! BRACell }.map(Utils.cellValue)
            let preCells: [String] = [testMatchValue, weirdText, "\(numOfTestFs)", matchValue]
            let exportRow: Row = preCells + cellsStr // arrived  | other cells
            exportRows.append(exportRow)
        }
        
        
        // Iterate all rows
        for row in dancersRows {

            guard row.cells.count > 7 else {
                print("short row: \(Utils.rowDescription(row))")
                continue
            }
            
            // is F row?
            guard (row.cells[4] as! BRACell).stringValue().uppercased() == "F"
                else { continue }
            
            let uniqueId = (row.cells[2] as! BRACell).stringValue()!
            
            if uniqueId != currentBeeTrialID {
                //new bee
                handleBeeInfo(fRows: currentBeeTrialIdFRows)
                
                currentBeeTrialIdFRows = []
                currentBeeTrialID = uniqueId
            }
            
            
            //print(row.rowIndex)
            
            // keep all F rows for this bee
            currentBeeTrialIdFRows.append(row)
        }
        
        export(rows: exportRows, onlyAfter)
    }
    
    class func export(rows: [Row], _ onlyAfter: Bool) {
        
        let excelRows: [ExcelExport.ExcelRow] = rows.enumerated().map{ rowIndex, row -> ExcelExport.ExcelRow in
            return row.enumerated().map{ index, value in
                var atts: [ExcelExport.TextAttribute] = rowIndex == 0 ? [.height(60), .bold] : []
                
                switch index {
                case 4: fallthrough
                case 9: fallthrough
                case 12:
                    atts.append(.format("[h]:mm:ss.000"))
                    
                case 13: fallthrough
                case 17:
                    atts.append(.format("dd-mmm-yy"))
                    
                default:
                    break
                }
                
                return ExcelExport.ExcelCell(value, atts)
            }
        }
        
        ExcelExport.export(rows: excelRows, fileName: "Followed Arrived Learning And Test") { _ in
            print("DONE! 🍻")
        }
        
    }
    
}
