//
//  main.swift
//  Excel
//
//  Created by Aviel Gross on 20/11/2016.
//  Copyright © 2016 i.am+. All rights reserved.
//

import Foundation

//defines
typealias Row = [String]


let path = "/Users/aviel/Developer/Private-xcode/excelexport-karmi/Excel/Excel/doc9.xlsx"
let doc = BRAOfficeDocumentPackage.open(path)

let dancers = doc!.workbook.worksheetNamed("Dancers")!
let feeders = doc!.workbook.worksheetNamed("Feeder VISITS (no exit)")!


//BuildDanceArrivals.run(dancers: dancers, feeders: feeders, onlyAfter: true)
//BuildDanceRate.run(dancersSheet: dancers)
//BuildDanceRateWithDuration.run(dancersSheet: dancers)
//BuildDanceArrivalsPerUniqueBee.run(dancers: dancers, feeders: feeders, onlyAfter: true)
BuildFollowedArrivedLearningAndTest.run(dancers: dancers, feeders: feeders, onlyAfter: true)
